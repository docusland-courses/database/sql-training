-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 18 oct. 2022 à 08:57
-- Version du serveur : 10.5.15-MariaDB-0+deb11u1
-- Version de PHP : 7.4.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mydb`
--

--
-- Déchargement des données de la table `City`
--

INSERT INTO `City` (`id`, `name`, `Country_id`) VALUES
(1, 'London', 1),
(2, 'Berlin', 2),
(3, 'Leeds', 1),
(4, 'York', 1),
(5, 'Manchester', 1),
(6, 'Leipzig', 2);

--
-- Déchargement des données de la table `Country`
--

INSERT INTO `Country` (`id`, `name`) VALUES
(1, 'Angleterre'),
(2, 'Allemagne');

--
-- Déchargement des données de la table `Housing`
--

INSERT INTO `Housing` (`id`, `name`, `adress`, `nb_rooms`, `nb_bedrooms`, `description`, `internal_ref`, `surface`, `indic_energy`, `indic_emission`, `has_garage`, `has_pool`, `has_garden`, `has_balcony`, `City_id`, `Owner_id`, `HousingType_id`) VALUES
(1, 'Maison allemande de 3 étages', '4 Lanwerstrasse', 6, 2, 'Belle maison sur étage...', '32165498A', '98.00', 153, 5, 1, 0, 0, 1, 6, 2, 2),
(2, 'Old Academical school', '13 Lanwerstrasse', 4, 1, 'Description d\'une belle maison allemande. Ancien collège réhabilité en lieu de vie.', '987654A32', '50.00', 42, 4, 0, 0, 1, 0, 6, 2, 2),
(3, 'Appartement Anglais', 'Downstreet 32', 7, 3, 'Nice flat', '00000000R', '60.00', 15, 9, 1, 1, 0, 0, 3, 1, 1),
(4, 'Appartement avec belle vue sur la ville de york', '1 shire street', 7, 7, 'Only sleeping rooms', '111111153A', '90.00', 8, 7, 0, 0, 0, 0, 4, 1, 1);

--
-- Déchargement des données de la table `HousingComment`
--

INSERT INTO `HousingComment` (`id`, `content`, `User_id`, `Housing_id`) VALUES
(1, 'Awesome flat !', 1, 4);

--
-- Déchargement des données de la table `HousingType`
--

INSERT INTO `HousingType` (`id`, `name`) VALUES
(1, 'Appartement'),
(2, 'Maison');

--
-- Déchargement des données de la table `housing_has_status`
--

INSERT INTO `housing_has_status` (`Status_id`, `Housing_id`, `date`) VALUES
(1, 1, '2022-10-02'),
(1, 2, '2022-10-03'),
(1, 2, '2022-10-11'),
(1, 3, '2022-10-04'),
(1, 4, '2022-10-06'),
(2, 2, '2022-10-09'),
(2, 4, '2022-10-11'),
(3, 2, '2022-10-17'),
(4, 3, '2022-10-18');

--
-- Déchargement des données de la table `Media`
--

INSERT INTO `Media` (`id`, `url`, `description`, `title`, `Housing_id`) VALUES
(1, 'http://campus.murraystate.edu/academic/faculty/BAtieh/House5.JPG', 'Front view', 'Front view', 2),
(2, 'http://landmarkhunter.com/photos/56/23/562368-L.jpg', 'Front view', 'Front view', 1),
(3, 'http://campus.murraystate.edu/academic/faculty/BAtieh/House2.JPG', 'Main door', 'Main door', 2);

--
-- Déchargement des données de la table `Owner`
--

INSERT INTO `Owner` (`id`, `firstname`, `lastname`, `gender`, `email`, `address`, `City_id`) VALUES
(1, 'James', 'Bradford', 'H', 'jean-michel.dupont@lilo.org', '6200 Grand River Blvd E', 3),
(2, 'Lutz', 'Laemmer', 'H', 'l.laemmer@protonmail.org', 'Dolivostraße 11', 6),
(7, 'Michael', 'Page', 'H', NULL, NULL, 6);

--
-- Déchargement des données de la table `Role`
--

INSERT INTO `Role` (`id`, `name`) VALUES
(1, 'Administrateur'),
(2, 'Utilisateur');

--
-- Déchargement des données de la table `Status`
--

INSERT INTO `Status` (`id`, `name`) VALUES
(4, 'Annulé'),
(1, 'Brouillon'),
(2, 'En vente'),
(3, 'Vendu');

--
-- Déchargement des données de la table `User`
--

INSERT INTO `User` (`id`, `login`, `password`, `Role_id`) VALUES
(1, 'agence_immo', 'password', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
